var searchData=
[
  ['lab0x00_3a_20preliminary_20hardware_20_26_20toolchain_20installation_58',['Lab0x00: Preliminary Hardware &amp; Toolchain Installation',['../page_lab0.html',1,'']]],
  ['lab0x01_2epy_59',['Lab0x01.py',['../Lab0x01_8py.html',1,'']]],
  ['lab0x01_3a_20vendotron_20fsm_20_26_20ui_20development_60',['Lab0x01: Vendotron FSM &amp; UI Development',['../page_lab1.html',1,'']]],
  ['lab0x02_3a_20think_20fast_21_20_28isrs_20_26_20timing_29_61',['Lab0x02: Think Fast! (ISRs &amp; Timing)',['../page_lab2.html',1,'']]],
  ['lab0x03_20pushing_20the_20right_20buttons_20_28adc_20_26_20isrs_29_62',['Lab0x03 Pushing the Right Buttons (ADC &amp; ISRs)',['../page_lab3.html',1,'']]],
  ['lab0x04_20hot_20or_20not_3f_20_28interfacing_20with_20sensors_20using_20i2c_29_63',['Lab0x04 Hot or Not? (Interfacing with Sensors using I2C)',['../page_lab4.html',1,'']]],
  ['lab0x04_2epy_64',['Lab0x04.py',['../Lab0x04_8py.html',1,'']]]
];
