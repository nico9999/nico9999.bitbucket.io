/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "My Project", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab0x00: Preliminary Hardware & Toolchain Installation", "page_lab0.html", [
      [ "Description", "page_lab0.html#page_lab0_desc", null ],
      [ "Documentation", "page_lab0.html#page_lab0_doc", null ]
    ] ],
    [ "Lab0x01: Vendotron FSM & UI Development", "page_lab1.html", [
      [ "Description", "page_lab1.html#page_lab1_desc", null ],
      [ "Finite State Machine", "page_lab1.html#page_lab1_fsm", null ],
      [ "Source Code", "page_lab1.html#page_lab1_source", null ],
      [ "Documentation", "page_lab1.html#page_lab1_doc", null ]
    ] ],
    [ "Lab0x02: Think Fast! (ISRs & Timing)", "page_lab2.html", [
      [ "Description", "page_lab2.html#page_lab2_desc", null ],
      [ "Source Code", "page_lab2.html#page_lab2_source", null ],
      [ "Documentation", "page_lab2.html#page_lab2_doc", null ]
    ] ],
    [ "HW0x02 Term Project System Modeling", "page_hw2.html", [
      [ "Description", "page_hw2.html#page_hw2_desc", null ]
    ] ],
    [ "Lab0x03 Pushing the Right Buttons (ADC & ISRs)", "page_lab3.html", [
      [ "Description", "page_lab3.html#page_lab4_desc", null ],
      [ "Documentation", "page_lab3.html#page_lab4_doc", null ]
    ] ],
    [ "HW0x04 Simulation or Reality? (Linearization and Simulation of System Model)", "page_hw4.html", [
      [ "Description", "page_hw4.html#page_hw4_desc", null ],
      [ "Open-Loop Simulink Model:", "page_hw4.html#page_hw4_OL", null ],
      [ "Closed-Loop Simulink Model:", "page_hw4.html#page_hw4_CL", null ],
      [ "Source Code", "page_hw4.html#page_hw4_source", null ]
    ] ],
    [ "Lab0x04 Hot or Not? (Interfacing with Sensors using I2C)", "page_lab4.html", null ],
    [ "Term Project: Balancing Platform", "page_TP.html", [
      [ "Description", "page_TP.html#page_TP_desc", null ],
      [ "Lessons Learned", "page_TP.html#page_TP_lessons_learned", null ],
      [ "Hardware", "page_TP.html#page_TP_hw", null ],
      [ "Base Mount", "page_TP.html#page_TP_baseMount", null ],
      [ "DC Motors", "page_TP.html#page_TP_Motor", null ],
      [ "Breakout Boards", "page_TP.html#page_TP_boards", null ],
      [ "Software", "page_TP.html#page_TP_soft", null ],
      [ "Task Queues and Shares", "page_TP.html#page_TP_shares", null ],
      [ "Drivers", "page_TP.html#page_TP_drivers", null ],
      [ "Documentation", "page_TP.html#page_TP_doc", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Encoder_8py.html",
"main_8py.html#af4b8f4290f8d32e70654f6deb864787f"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';